Function Get-WallhavenImages {
    # Change $maxPages on line 29 if you want to set your own amount of max pages (it can get a lot of images)
    # Add WebObjects into $webURLs as [URI] objects
    
    param (
            [Parameter(ValueFromPipeline)][Parameter(Mandatory)]
            [Array]$webURLs, 
            [String]$password, 
            [String]$username,  
            [String]$OutDir = ".\",
            [Int32]$maxPages = 0 ,
            [Int32]$StartPage=1, 
            [String]$JsonLoadPath="", 
            [String]$XmlSavePath=",\WallHavenDatabase.xml", 
            [Boolean]$OnlyGetTags=$false,
            [Boolean]$GetTags = $true, 
            [Boolean]$ExportJson = $false,
            [Int32]$waittime=500
        )

    if ((!($username -eq $null)) -and (!($password -eq $null))) {
        $login = Invoke-WebRequest -Uri https://alpha.wallhaven.cc/auth/login -Body @{"username"=$username;"password"=$password} -Method Post -SessionVariable 'Session' -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"
    } else {
        $login = Invoke-WebRequest -Uri https://alpha.wallhaven.cc/auth/login-SessionVariable 'Session' -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"
    }

    $webURLs = $webURLs
    $dlURLs = @()
    $setMaxPages = $maxPages
    $OutDir = $OutDir
    
    $jsondb = @{"TagNames" = @{};
            "TagCatigories" = @{};
            "TagClassification" = @{
                "NSFW" = @();
                "Sketchy"=@();
                "SFW"=@();
                };
            "ImageTags" = @{};
            "ImageClassification" = @{
                "NSFW" = @();
                "Sketchy"=@();
                "SFW"=@()
                };
            };
    if (!($JsonLoadPath -eq "")) {
        $jsondb = (Import-Clixml -Path .\text.xml)
    }

    if (!(Test-Path ("$outdir"))) {
        New-Item -ItemType "directory" ("$outdir")
    }
    
    # Image Tag Scraper
    Function Get-ImageTag ($WebObject3) {  
        $opas = @()
        for ($r=0; $r -le ($WebObject3.Links.Count-1);$r++) {
            if ($WebObject3.Links[$r].rel -imatch "(tag)") {
                $TagID = $WebObject3.Links[$r].href.Split("/")[-1]
                $TagName = (($WebObject3.Links[$r] -split "(`">)") -split"</a>")[-2]
                $PictureID = $WebObject3.BaseResponse.ResponseUri.AbsolutePath.Split("/")[-1]
                $Classification = ((($WebObject3.Content -split "(purity )") -isplit "(</label></form><dl>)")[-3] -split "`">")[-1]
             
                $opas += , $tagName

                if(!($jsondb.TagNames.ContainsKey(("ID-"+$TagID)))) {
                    $jsondb.TagNames.("ID-"+$TagID) = $TagName
                }
                if(!($jsondb.ImageTags.ContainsKey(("ID-"+$TagID)))) {
                    $jsondb.ImageTags.("ID-"+$TagID) = @()
                }
                if (!($jsondb.ImageTags.("ID-"+$TagID).Contains($PictureID))) {
                    $jsondb.ImageTags.("ID-"+$TagID) += $PictureID
                }
                if (!($jsondb.ImageClassification.$Classification.Contains($PictureID))) {
                    $jsondb.ImageClassification.$Classification += $PictureID
                }
            }
        }
        for ($p=0; $p -le ($opas.Count-1);$p++) {
            if (!($p -eq $tags.Count-1)) {
                $opas[$p] = $opas[$p]+","
            }
        }
        return $opas
    }

    # Image Downloader
    Function Image-Download($URI) {
        $OutDir = $URI[1]
        $successfull = $false
        $outName = [String]($URI[0].Split("/"))[-1]
        if(!(Test-Path($OutDir+"\"+$outName))) {
            wget $URI[0] -UseBasicParsing -OutFile $OutDir"\"$outName -WebSession $Session
            sleep -Milliseconds $waittime
            if ((Test-Path ($OutDir+"\"+$outName))) {
                $successfull = $true
            }
        }
        return $successfull
    }
    
    # Image src address grabber
    Function Get-Images($WebObject) {
        $OutDir = $WebObject[1]
        for($x=0; $x -le ($WebObject[0].Links.Count-1);$x++){
            if ($WebObject.Links[$x].href -match "(https:\/\/alpha.wallhaven.cc\/wallpaper\/)((\d)+$)") {
                if ($OnlyGetTags -eq $true) {
                    $WebObject = wget $WebObject.Links[$x].href -UseBasicParsing -WebSession $Session
                    $tags = Get-ImageTag($WebObject)
                    Write-Host ([String]("Picture ID: " + ($WebObject.Links[$x].href).Split("/")[-1] + " had Tags: " + $tags))
                } elseif ((!(Test-Path ($OutDir+"\wallhaven-"+$WebObject.Links[$x].href.Split("/")[-1]+".png"))) -and (!(Test-Path ($OutDir+"\wallhaven-"+($WebObject.Links[$x]).href.Split("/")[-1]+".jpg")))) {
                        $WebObject2 = wget $WebObject.Links[$x].href -UseBasicParsing -WebSession $Session
                        if ($GetTags) {
                            $tags = Get-ImageTag($WebObject2)
                            Write-Host ("Downloaded Picture ID: " + ($WebObject.Links[$x].href).Split("/")[-1] + " With Tags: " + [String]$tags)
                        } else {
                            Write-Host ("Downloaded Picture ID: " + ($WebObject.Links[$x].href).Split("/")[-1])
                        }
                        $downloaded = Image-Download([String]("http:"+$WebObject2.Images[-1].src), $OutDir)
                        if ($downloaded) {
                            $successfullamount++
                        }  
                    } else {
                        Write-Host ("Picture ID: " + $WebObject.Links[$x].href.Split("/")[-1] + " all ready Downloaded")
                    }
                
            }
           
        }
        return $successfullamount
    }
    
    
    # WebObject Scraper
    for ($a=0; $a -le ($webURLs.Length-1);$a++){
        $WebObject = wget $webURLs[$a] -UseBasicParsing -WebSession $Session
        if ($setMaxPages -eq 0) {
            if ($WebObject.Links[-7].title -replace "Page " -eq "") {
                 $maxPages = 1
            } else {
                $maxPages = $WebObject.Links[-7].title -replace "Page"
            }
        } else {
            $maxPages = $setMaxPages
        }
        Write-Host ([String]($a+1) + "/" + [String]($webURLs.Length) +"`: " + $WebObject.BaseResponse.ResponseUri.OriginalString)
        for($page=$StartPage; $page -le $maxPages;$page++) {
            if (!($page -eq 1)) {
                if($webURLs[$a] -match "(.)*(&)(.)*") {
                    $WebObject = wget ([String]($webURLs[$a] + "&page=" + [String]$page)) -UseBasicParsing -WebSession $Session
                } else {
                    $WebObject = wget ([String]($webURLs[$a] + "?page=" + [String]$page)) -UseBasicParsing -WebSession $Session

                }
            } else {
                $WebObject = wget ([String]($webURLs[$a])) -UseBasicParsing -WebSession $Session
            }
            Write-Host ("Getting Images from Page:"+$page+"/"+$maxpages)
            $downloadedpics += Get-Images($WebObject,$OutDir)
            sleep -Milliseconds $waittime
            ("[x] Page "+$page+" of link ("+($a+1) + "/" + [String]($webURLs.Length)+"): "+$WebObject.BaseResponse.ResponseUri.OriginalString) | Out-File -Append ".\pageprogress.txt"
        }
    }
    Write-Host ("[#] "+$downloadedpics+" - Picture's were successfully downloaded from "+$pages+" pages to "+$outdir)
    Write-Host ("[#] Saving TagData Database to " + $XmlSavePath)
    Export-Clixml -InputObject $jsondb -Path $XmlSavePath
    if ($ExportJson) {
        ConvertTo-Json $jsondb | Out-File .\jsonOut.json
    }

}

Export-ModuleMember Get-WallhavenImages