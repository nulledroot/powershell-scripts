$jsondb = @{"TagNames" = @{};
            "TagCatigories" = @{};
            "TagClassification" = @{
                "NSFW" = @();
                "Sketchy"=@();
                "SFW"=@();
                };
            "ImageTags" = @{};
            "ImageClassification" = @{
                "NSFW" = @();
                "Sketchy"=@();
                "SFW"=@()
                };
            };

$username = ""
$password = ""
$XmlSavePath = ".\WallhavenDB.xml"


$login = Invoke-WebRequest -Uri https://alpha.wallhaven.cc/auth/login -Body @{"username"=$username;"password"=$password} -Method Post -SessionVariable 'Session' -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"

# TagDB Builder
$WebItem = Invoke-WebRequest "https://alpha.wallhaven.cc/tags" -UseBasicParsing -WebSession $Session
for ($a=1; $a -le 9;$a++) {
    $TagID = $a
    if(!($jsondb.TagCatigories.ContainsKey(("ID-"+$TagID)))) {
            $jsondb.TagCatigories.Add(('ID-'+$TagID),@{})
    }
}

for ($a=1; $a -le 9;$a++) {
    $WebItem = Invoke-WebRequest ("https://alpha.wallhaven.cc/tags/"+$a) -UseBasicParsing -WebSession $Session
    $ParentCategotyName = ((($WebItem.Content -split "(<header class=`"col-nav-header`"><h2>  )") -split "(  </h2><nav class=`"nav-col nav-col-2`">)")[-3]) -replace "(&amp;)", "&"
    $work = ($WebItem.Content -split "(<nav class=`"nav-col nav-col-2`">  <ul>  <li>)" -split "((<small>)(\d)*(</small> </a> </li>  </ul>  </nav></header>))")[-6]
    $CategoryArray = $work -split "(`">)" -split "(<a href=`")" -replace  "((<small>)(\d){1,}(</small> </a> </li>  <li> ))" -replace "(<a href=`")" -replace "(`">)"
    $ParentCategotyID = $a
    Write-Host ("Getting Tags from Tag Category: "+$ParentCategotyName)
    if(!($jsondb.TagNames.ContainsKey(("ID-"+$a)))) {
                $jsondb.TagNames.("ID-"+$a) = $ParentCategotyName
    }
    if(!($jsondb.TagCatigories.ContainsKey(("ID-"+$a)))) {
                $jsondb.TagNames.("ID-"+$a) = @{}
    }

    for ($b=0; $b -le ($CategoryArray.Length-1);$b++) {
        if ($CategoryArray[$b] -match "((https:\/\/alpha.wallhaven.cc\/tags\/)(\d)+$)") {
            $URI = $CategoryArray[$b]
            $CategoryTagID = $URI.Split("/")[-1]
            $CategoryName = $CategoryArray[$b+2].Substring(1)
            $WebItem = Invoke-WebRequest ("https://alpha.wallhaven.cc/tags/"+$CategoryTagID) -UseBasicParsing
            $maxPages = $WebItem.Links[-7].href.Split("=")[-1]
            Write-Host ("Getting Tags from Tags Sub-Category: "+$CategoryName)
        
            if(!($jsondb.TagNames.ContainsKey(("ID-"+$CategoryTagID)))) {
                    $jsondb.TagNames.("ID-"+$CategoryTagID) = $CategoryName
            }
            if(!($jsondb.TagCatigories.("ID-"+$ParentCategotyID).ContainsKey(("ID-"+$CategoryTagID)))) {
                    $jsondb.TagCatigories.("ID-"+$ParentCategotyID).("ID-"+$CategoryTagID) = @()
            }

            for($page=1; $page -le $maxPages;$page++) {
                Write-Host ("Getting Tags from Page: "+$page+"/"+$maxpages)
                if (!($page -eq 1)) {
                    if($URI -match "(.)*(&)(.)*") {
                        $WebItem = wget ([String]($URI + "&page=" + [String]$page)) -UseBasicParsing -WebSession $Session
                    } else {
                        $WebItem = wget ([String]($URI + "?page=" + [String]$page)) -UseBasicParsing -WebSession $Session

                    }
                } else {
                    $WebItem = wget ([String]($URI)) -UseBasicParsing -WebSession $Session
                }
                for ($linkno=0; $linkno -le ($WebItem.Links.Count-1);$linkno++) {
                    if ($WebItem.Links[$linkno].class -match "((taglist-name )(.)*)") {
                    
                    
                        $TagClassification = $WebItem.Links[$linkno].class.Split(" ")[-1]
                        $TagName = ($WebItem.Links[$linkno].outerHTML -split "(</a>)" -split "(`" >)")[-3]
                        if ($TagName.Contains("</span>")) {$TagName = ($TagName -split "(<span)" -split "(</span> )")}
                        if (($TagName.GetType()).BaseType.Name -eq "Array") {$TagName = ($TagName[0] + $TagName[-1])}
                        $TagID = $WebItem.Links[$linkno].href.Split("/")[-1]

                        Write-Host ("Got Tag: " + $TagName + " with the TagID of " + $TagID + " and classification of " + $TagClassification)

                        if(!($jsondb.TagNames.ContainsKey(("ID-"+$TagID)))) {
                            $jsondb.TagNames.("ID-"+$TagID) = $TagName
                        }
                        if(!($jsondb.TagCatigories.("ID-"+$ParentCategotyID).("ID-"+$CategoryTagID).Contains(("ID-"+$TagID)))) {
                            $jsondb.TagCatigories.("ID-"+$ParentCategotyID).("ID-"+$CategoryTagID) += ("ID-"+$TagID)
                        }
                        if (!($jsondb.TagClassification.$TagClassification.Contains($TagID))) {
                            $jsondb.TagClassification.$TagClassification += $TagID
                        }
                    }
                }
            }
        }
    }
}
Export-Clixml -InputObject $jsondb -Path $XmlSavePath

# Image Tag Scraper
Function Get-ImageTag ($WebItem) {  
        for ($a=0; $a -le ($WebItem.Links.Count-1);$a++) {
            if ($WebItem.Links[$a].rel -imatch "(tag)") {
                $tags = @()
                $TagID = $WebItem.Links[$a].href.Split("/")[-1]
                $TagName = (($WebItem.Links[$a] -split "(`">)") -split"</a>")[-2]
                $PictureID = $WebItem.BaseResponse.ResponseUri.AbsolutePath.Split("/")[-1]
                $Classification = ((($test.Content -split "(purity )") -isplit "(</label></form><dl>)")[-3] -split "`">")[-1]
                
                $tags += $tagName

                if(!($jsondb.TagNames.ContainsKey(("ID-"+$TagID)))) {
                    $jsondb.TagNames.("ID-"+$TagID) = $TagName
                }
                if(!($jsondb.ImageTags.ContainsKey(("ID-"+$TagID)))) {
                    $jsondb.ImageTags.("ID-"+$TagID) = @()
                }
                if (!($jsondb.ImageTags.("ID-"+$TagID).Contains($PictureID))) {
                    $jsondb.ImageTags.("ID-"+$TagID) += $PictureID
                }
                if (!($jsondb.ImageClassification.$Classification.Contains($PictureID))) {
                    $jsondb.ImageClassification.$Classification += $PictureID
                }
            }
        }
        return $tags
    }

#ConvertTo-Json $jsondb